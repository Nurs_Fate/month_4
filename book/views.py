from django.shortcuts import render
from . import models


def book_views(request):
    book = models.BookProgramLang.objects.all()
    return render(request, 'book/book.html', {'book': book})

