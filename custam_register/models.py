from django.db import models
from django.contrib.auth.models import User


class CustomUser(User):
    USER_TYPE = (
        ('Client', 'Client'),
        ('VipClient', 'VipClient'),
        ('BigBoss', 'BigBoss')
    )

    GENDER = (
        ('M', 'M'),
        ('Ж', 'Ж')
    )

    user_type = models.CharField(max_length=100, choices=USER_TYPE)
    phone_number = models.CharField(max_length=13, default="+996",)
    age = models.PositiveIntegerField(default=18)
    gender = models.CharField(max_length=100, choices=GENDER)
    address = models.CharField(max_length=255)
    country = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    language = models.CharField(max_length=100, blank=True)
    postal_code = models.CharField(max_length=10)
    occupation = models.CharField(max_length=100)
    education = models.CharField(max_length=100)
    hobbies = models.TextField(blank=True)
    favorite_books = models.TextField(blank=True)
    social_media = models.CharField(max_length=100, blank=True)
    photo = models.ImageField(upload_to=True, null=True)

