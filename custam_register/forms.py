from django import forms
from django.contrib.auth.forms import UserCreationForm
from . import models

USER_TYPE = (
    ('Client', 'Client'),
    ('VipClient', 'VipClient'),
    ('BigBoss', 'BigBoss')
)

GENDER = (
    ('M', 'M'),
    ('Ж', 'Ж')
)


class CustomUserRegistration(UserCreationForm):
    email = forms.EmailField(required=True)
    phone_number = forms.CharField(required=True)
    gender = forms.ChoiceField(choices=GENDER, required=True)
    user_type = forms.ChoiceField(choices=USER_TYPE, required=True)
    address = forms.CharField(required=True)
    country = forms.CharField(required=True)
    city = forms.CharField(required=True)
    postal_code = forms.CharField(required=True)
    occupation = forms.CharField(required=True)
    education = forms.CharField(required=True)
    social_media = forms.CharField(required=False)
    language = forms.CharField(required=False)
    favorite_books = forms.CharField(widget=forms.Textarea, required=False)
    hobbies = forms.CharField(widget=forms.Textarea, required=False)
    photo = forms.ImageField(required=True)

    class Meta:
        model = models.CustomUser
        fields = (
            "username",
            "email",
            "password1",
            "password2",
            "phone_number",
            "age",
            "gender",
            "user_type",
            "address",
            "country",
            "city",
            "postal_code",
            "occupation",
            "education",
            "social_media",
            "language",
            "favorite_books",
            "hobbies",
            "photo",
        )

    def save(self, commit=True):
        user = super(CustomUserRegistration, self).save(commit=False)
        user.email = self.cleaned_data['email']
        if commit:
            user.save()
        return user
