from django.apps import AppConfig


class CustamRegisterConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'custam_register'
