from pprint import pprint

import requests
from bs4 import BeautifulSoup as BS
from django.views.decorators.csrf import csrf_exempt

URL = 'https://www.mashina.kg/'

HEADERS = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36",
}


@csrf_exempt
def get_html(url, params=""):
    req = requests.get(url, headers=HEADERS, params=params)
    return req


@csrf_exempt
def get_data(html):
    soup = BS(html, "html.parser")
    items = soup.find_all("div", class_="list-item list-label")
    car = []

    for item in items:
        car.append(
            {
                'title_text': item.find("h2", class_='name').get_text().replace(" ", ""),
                'title_url': URL + item.find("a").get('href'),
                'image': item.find("img", class_='tmb-wrap-table')
            }
        )
    return car

# @csrf_exempt
def parser():
    html = get_html(URL)
    if html.status_code == 200:
        cars = []
        for page in range(0, 1):
            html = get_html(f"https://www.mashina.kg/search/all/", params=page)
            cars.extend(get_data(html.text))
            # pprint(cars)
            return cars

    else:
        raise Exception('gvfvfvfv')


# parser()
