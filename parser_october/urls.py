from django.urls import path
from . import views

urlpatterns = [
    path('start_car/', views.ParserFormView.as_view(), name='start_car'),
    path('car_list/', views.FilmListView.as_view(), name='car_list'),
]
